set nocompatible

filetype off

execute pathogen#infect()
" UI config
set number
set showmatch
set nowrap
set colorcolumn=80

set splitbelow
set splitright

set tabstop=4
set softtabstop=0
set expandtab
set shiftwidth=4
set smarttab

syntax on
filetype plugin indent on
let g:solarized_termcolors=256
let g:molokai_original = 1
let g:rehash256 = 1
colorscheme molokai

au BufNewFile,BufRead *.k8s set syntax=yaml

map <C-n> :NERDTreeToggle<CR>

